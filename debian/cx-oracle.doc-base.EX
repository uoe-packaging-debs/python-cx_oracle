Document: cx-oracle
Title: Debian cx-oracle Manual
Author: <insert document author here>
Abstract: This manual describes what cx-oracle is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/cx-oracle/cx-oracle.sgml.gz

Format: postscript
Files: /usr/share/doc/cx-oracle/cx-oracle.ps.gz

Format: text
Files: /usr/share/doc/cx-oracle/cx-oracle.text.gz

Format: HTML
Index: /usr/share/doc/cx-oracle/html/index.html
Files: /usr/share/doc/cx-oracle/html/*.html
